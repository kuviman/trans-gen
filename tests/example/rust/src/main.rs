use std::fs::File;
use trans::Trans;

fn main() {
    let example_object: model::Example =
        Trans::read_from(&mut File::open("example.trans").unwrap()).unwrap();
    println!("{:#?}", example_object);
    example_object
        .write_to(&mut File::create("example-rust.trans").unwrap())
        .unwrap();
}
