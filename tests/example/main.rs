use std::collections::HashMap;
use std::fs::File;

use std::io;
use std::path::{Path, PathBuf};
#[derive(trans::Trans, trans_schema::Schematic, PartialEq, Debug)]
struct Example {
    f_bool: bool,
    f_i32: i32,
    f_i64: i64,
    f_f32: f32,
    f_f64: f64,
    f_option_some: Option<ExampleInner>,
    f_option_none: Option<bool>,
    f_newtype: Newtype<i32>,
    f_option_1: OneOf,
    f_option_2: OneOf,
    f_vec: Vec<usize>,
    f_map: HashMap<i32, bool>,
}

#[derive(trans::Trans, trans_schema::Schematic, PartialEq, Debug)]
struct Newtype<T>(T);

#[derive(trans::Trans, trans_schema::Schematic, PartialEq, Debug)]
struct ExampleInner {
    f_string: String,
}

#[derive(trans::Trans, trans_schema::Schematic, PartialEq, Debug)]
enum OneOf {
    OptionOne { a: i32 },
    OptionTwo { b: bool },
}

fn test_with(f: fn(&Path) -> PathBuf) {
    let dir = tempfile::tempdir().unwrap();
    let dir = dir.path();

    let example_object = Example {
        f_bool: true,
        f_i32: 1,
        f_i64: 2,
        f_f32: 3.0,
        f_f64: 4.0,
        f_option_some: Some(ExampleInner {
            f_string: "Five".to_owned(),
        }),
        f_option_none: None,
        f_newtype: Newtype(123),
        f_option_1: OneOf::OptionOne { a: 321 },
        f_option_2: OneOf::OptionTwo { b: true },
        f_vec: vec![7, 11, 13, 19],
        f_map: {
            let mut map = HashMap::new();
            map.insert(1, false);
            map.insert(2, true);
            map.insert(3, true);
            map.insert(4, false);
            map.insert(5, true);
            map
        },
    };
    let example_file = dir.join("example.trans");
    trans::Trans::write_to(&example_object, &mut File::create(&example_file).unwrap()).unwrap();

    assert_eq!(
        example_object,
        trans::Trans::read_from(&mut File::open(f(dir)).expect("Failed to open file"))
            .expect("Failed to read back")
    );
}

#[test]
#[ignore]
fn test_python() {
    test_with(|dir| {
        use std::io::Write;
        let mut gen = trans_gen::Python::new("test_python", "0.1");
        gen.add(&trans_schema::schema::<Example>());
        gen.write_to(dir.join("python")).unwrap();
        File::create(dir.join("python").join("test.py"))
            .unwrap()
            .write_all(include_str!("python/test.py").as_bytes())
            .unwrap();
        let mut command;
        #[cfg(windows)]
        {
            command = std::process::Command::new("py");
            command.arg("-3");
        }
        #[cfg(not(windows))]
        {
            command = std::process::Command::new("python3");
        }
        command.arg("python/test.py");
        command.current_dir(dir);
        assert!(command.status().unwrap().success());
        dir.join("example-python.trans")
    });
}

#[test]
#[ignore]
fn test_rust() {
    test_with(|dir| {
        use std::io::Write;
        let mut gen = trans_gen::Rust::new("trans-example-model", "0.1.0");
        gen.add(&trans_schema::schema::<Example>());
        gen.write_to(dir.join("model")).unwrap();
        std::fs::create_dir_all(dir.join("src")).unwrap();
        File::create(dir.join("src").join("main.rs"))
            .unwrap()
            .write_all(include_str!("rust/src/main.rs").as_bytes())
            .unwrap();
        File::create(dir.join("Cargo.toml"))
            .unwrap()
            .write_all(include_str!("rust/Cargo.toml.template").as_bytes())
            .unwrap();
        let mut command = std::process::Command::new("cargo");
        command.arg("run");
        command.current_dir(dir);
        assert!(command.status().unwrap().success());
        dir.join("example-rust.trans")
    });
}
