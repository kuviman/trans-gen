import model
from stream_wrapper import StreamWrapper

reader = StreamWrapper(open("example.trans", "rb"))
exampleObject = model.Example.read_from(reader)
reader.close()

print(exampleObject)

writer = StreamWrapper(open("example-python.trans", "wb"))
exampleObject.write_to(writer)
writer.close()
